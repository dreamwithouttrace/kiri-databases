<?php


use Database\Query;

class Users extends \Database\Model
{


	public function hasD()
	{
		return $this->hasOne(static::class, 'id', 'id')->with([]);
	}
}

Users::query()
	->select(['*', (new Query(Users::class))
		->where(['id' => 2])])
	->from(function (Query $query) {
		$query->from(Users::class)
			->where(['id' => 1])
			->groupBy('name DESC');
	})->toSql();
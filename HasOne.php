<?php
/**
 * Created by PhpStorm.
 * User: whwyy
 * Date: 2018/4/4 0004
 * Time: 13:47
 */
declare(strict_types=1);

namespace Database;

use Database\Traits\HasBase;
use Exception;
use Kiri;
use Kiri\Di\Context;

/**
 * Class HasOne
 * @package Database
 * @internal Query
 */
class HasOne extends HasBase
{

    /**
     * @return array|null|ModelInterface
     * @throws
     */
    public function get(): array|ModelInterface|null
    {
        return di(Relation::class)->first($this->name);
    }
}

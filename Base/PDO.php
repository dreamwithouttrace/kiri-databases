<?php

namespace Database\Base;

class PDO extends \PDO
{


    /**
     * @param string $database
     * @param string $host
     * @param string $username
     * @param string $password
     * @param array $options
     */
    public function __construct(
        readonly public string $database,
        readonly public string $host,
        readonly public string $username,
        readonly public string $password,
        readonly public array  $options,
    )
    {
        parent::__construct('mysql:dbname=' . $this->database . ';host=' . $this->host, $this->username, $this->password, $this->options);
    }


}

<?php
/** @noinspection ALL */

declare(strict_types=1);

namespace Database;


use JetBrains\PhpStorm\Pure;
use Kiri;
use Kiri\Abstracts\Component;


/**
 * Class SqlBuilder
 * @package Database
 */
class SqlBuilder extends Component
{

    /**
     * @var ActiveQuery|Query|ISqlBuilder|null
     */
    public ActiveQuery|Query|ISqlBuilder|null $query;


    /**
     * @param ActiveQuery|Query|ISqlBuilder|null $config
     */
    public function __construct(ActiveQuery|Query|null|ISqlBuilder $config)
    {
        parent::__construct();

        $this->query = $config;
    }


    /**
     * @param ISqlBuilder|null $query
     * @return $this
     * @throws
     */
    public static function builder(ISqlBuilder|null $query): static
    {
        return new static($query);
    }


    /**
     * @return string
     * @throws
     */
    public function getCondition(): string
    {
        return $this->where($this->query->getWhere());
    }


    /**
     * @param array $compiler
     * @return string
     * @throws
     */
    public function hashCompiler(array $compiler): string
    {
        return $this->where($compiler);
    }


    /**
     * @param array $attributes
     * @return bool|array
     * @throws
     */
    public function update(array $attributes): bool|string
    {
        $conditions = $this->query->getParams();
        $this->query->setParams([]);
        $data = $this->__updateBuilder($this->makeParams($attributes));
        foreach ($conditions as $condition) {
            $this->query->pushParam($condition);
        }
        return $data;
    }


    /**
     * @param array $attributes
     * @param string $opera
     * @return bool|array
     * @throws
     */
    public function mathematics(array $attributes, string $opera = '+'): bool|string
    {
        $string = [];
        foreach ($attributes as $attribute => $value) {
            $string[] = $attribute . '=' . $attribute . $opera . $value;
        }
        return $this->__updateBuilder($string);
    }


    /**
     * @param array $string
     * @return string|bool
     * @throws
     */
    private function __updateBuilder(array $string): string|bool
    {
        if (empty($string)) {
            return Kiri::getLogger()->failure('None data update.');
        }
        return 'UPDATE ' . $this->query->getFrom() . ' SET ' . implode(',', $string) . $this->make();
    }


    /**
     * @param array $attributes
     * @param false $isBatch
     * @return array
     * @throws
     */
    public function insert(array $attributes, bool $isBatch = false): string
    {
        $update = 'INSERT INTO ' . $this->query->getFrom();
        if ($isBatch === false) {
            $attributes = [$attributes];
        }
        $update .= '(' . implode(',', $this->getFields($attributes)) . ') VALUES ';

        $keys = [];
        foreach ($attributes as $attribute) {
            $_keys = $this->makeParams($attribute, true);

            $keys[] = implode(',', $_keys);
        }
        return $update . '(' . implode('),(', $keys) . ')';
    }


    /**
     * @return string
     * @throws
     */
    public function delete(): string
    {
        return 'DELETE FROM ' . $this->query->getFrom() . $this->make();
    }


    /**
     * @param $attributes
     * @return array
     */
    #[Pure] private function getFields(array $attributes): array
    {
        return array_keys(current($attributes));
    }


    /**
     * @param array $attributes
     * @param bool $isInsert
     * @return array[]
     * a=:b,
     */
    private function makeParams(array $attributes, bool $isInsert = false): array
    {
        $keys = [];
        foreach ($attributes as $key => $value) {
            if ($isInsert === true) {
                $keys[] = '?';
                $this->query->pushParam($value);
            } else {
                $keys = $this->resolveParams($key, $value, $keys);
            }
        }
        return $keys;
    }


    /**
     * @param string $key
     * @param mixed $value
     * @param array $keys
     * @return array
     */
    private function resolveParams(string $key, mixed $value, array $keys): array
    {
        if (is_null($value)) {
            return $keys;
        }
        if (preg_match('/^[+|-]\s\d+$/', (string)$value)) {
            $keys[] = $key . '=' . $key . ' ' . $value;
        } else {
            $this->query->pushParam($value);
            $keys[] = $key . '= ?';
        }
        return $keys;
    }


    /**
     * @return string
     * @throws
     */
    public function one(): string
    {
        return $this->makeSelect($this->query->getSelect()) . $this->make() . $this->makeLimit($this->query->limit(1));
    }


    /**
     * @return string
     * @throws
     */
    public function all(): string
    {
        return $this->makeSelect($this->query->getSelect()) . $this->make() . $this->makeLimit($this->query);
    }


    /**
     * @return string
     * @throws
     */
    public function count(): string
    {
        return $this->makeSelect(['COUNT(*)']) . $this->make();
    }


    /**
     * @return string
     * @throws
     */
    public function exists(): string
    {
        return $this->makeSelect(['0']) . $this->make();
    }


    /**
     * @param string $table
     * @return string
     */
    public function columns(string $table): string
    {
        return 'SHOW FULL FIELDS FROM ' . $table;
    }


    /**
     * @return string
     * @throws
     */
    private function make(): string
    {
        $select = $this->makeCondition();
        $select .= $this->makeGroup();
        $select .= $this->makeOrder();
        return $select;
    }

    /**
     * @param array $select
     * @return string
     */
    private function makeSelect(array $select = ['*']): string
    {
        $select = "SELECT " . implode(',', $select) . " FROM " . $this->query->getFrom();
        if ($this->query->getAlias() != "") {
            $select .= " AS " . $this->query->getAlias();
        }
        if (count($this->query->getJoin()) > 0) {
            $select .= ' ' . implode(' ', $this->query->getJoin());
        }
        return $select;
    }


    /**
     * @return string
     */
    private function makeGroup(): string
    {
        if ($this->query->getGroup() != "") {
            return ' GROUP BY ' . $this->query->getGroup();
        }
        return '';
    }


    /**
     * @return string
     */
    private function makeOrder(): string
    {
        if (count($this->query->getOrder()) > 0) {
            return ' ORDER BY ' . implode(',', $this->query->getOrder());
        }
        return '';
    }


    /**
     * @return string
     */
    private function makeCondition(): string
    {
        $condition = $this->where($this->query->getWhere());
        if (empty($condition)) {
            return '';
        }
        return ' WHERE ' . $condition;
    }


    private function makeLimit(): string
    {
        if ($this->query->getOffset() >= 0 && $this->query->getLimit() >= 1) {
            return ' LIMIT ' . $this->query->getOffset() . ',' . $this->query->getLimit();
        }
        return '';
    }


    /**
     * @param false $isCount
     * @return string
     * @throws
     */
    public function get(bool $isCount = false): string
    {
        if ($isCount === false) {
            return $this->all();
        }
        return $this->count();
    }


    /**
     * @return string
     * @throws
     */
    public function truncate(): string
    {
        return sprintf('TRUNCATE %s', $this->query->getFrom());
    }


    /**
     * @param array $where
     * @return string
     */
    private function where(array $where): string
    {
        if (count($where) < 1) {
            return '';
        }
        $_tmp = [];
        foreach ($where as $key => $value) {
            $_tmp[] = $this->resolveCondition($key, $value);
        }
        return implode(' AND ', $_tmp);
    }


    /**
     * @param string $field
     * @param mixed $condition
     * @return string
     */
    private function resolveCondition(string|int $field, mixed $condition): string
    {
        if (is_string($field)) {
            $this->query->pushParam($condition);
            return $field . ' = ?';
        } else if (is_string($condition)) {
            return $condition;
        } else {
            return implode(' AND ', $this->_hashMap($condition));
        }
    }


    /**
     * @param $condition
     * @return array
     */
    private function _hashMap(array $condition): array
    {
        $_array = [];
        foreach ($condition as $key => $value) {
            $this->query->pushParam($value);
            $_array[] = $key . '= ?';
        }
        return $_array;
    }


}

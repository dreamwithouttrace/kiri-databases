<?php
declare(strict_types=1);

namespace Database;

use Database\Traits\HasBase;
use Kiri;
use Kiri\Di\Context;

/**
 * Class HasCount
 * @package Database
 */
class HasCount extends HasBase
{

    /**
     * @return array|null|ModelInterface
     * @throws
     */
    public function get(): array|ModelInterface|null
    {
        return di(Relation::class)->get($this->name);
    }

}
